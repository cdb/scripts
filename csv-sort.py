#!/usr/bin/env python3

import argparse
import csv
import logging
import operator
import sys


def main():
    logging.basicConfig(
        level=logging.INFO, format="%(asctime)s %(levelname)s %(message)s"
    )

    parser = argparse.ArgumentParser(
        description="""
Sort a CSV file.
            """,
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    parser.add_argument(
        "--key",
        "-k",
        type=str,
        required=True,
        action="append",
        help="columns to sort by, specify multiple times for multiple keys",
    )

    parser.add_argument("--quiet", "-q", action="store_true", help="quiet output")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")

    args = parser.parse_args()

    if args.quiet:
        logging.getLogger().setLevel(logging.WARNING)

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    logging.debug(f"sort keys: {args.key}")

    reader = csv.DictReader(sys.stdin)
    logging.debug(f"field names: {reader.fieldnames}")

    rows = sorted(reader, key=operator.itemgetter(*args.key))
    logging.debug(f"sorted {len(rows)} rows")

    writer = csv.DictWriter(sys.stdout, fieldnames=reader.fieldnames)
    writer.writeheader()
    writer.writerows(rows)
    logging.debug(f"wrote {len(rows)} rows")


if __name__ == "__main__":
    main()
