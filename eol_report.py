#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Produce a report on the line endings (EOL) a tree of files, CR/LF/CRLF.
# Colin D Bennett, 2013-07-03.

import argparse
import os
import sys

def main():
    parser = argparse.ArgumentParser(
            description="Produce a report on EOL types for file tree.")

    parser.add_argument("paths", nargs="+", help="specify file/directory paths")
    parser.add_argument("--quiet", action="store_true", help="quiet")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")

    args = parser.parse_args()

    stats = EolStats()
    for path in args.paths:
        stats.process(path)


class EolStats(object):
    def __init__(self):
        sys.stdout.write("%6s %6s %6s  %6s  %s\n" % ("LF", "CRLF", "CR", "Type", "File"))

    def process(self, rootpath):
        if rootpath == "-":
            scan_eols_stream(sys.stdin, "<stdin>")
        elif os.path.isdir(rootpath):
            # It's a directory, process the files in it recursively.
            for dirpath, dirnames, files in os.walk(rootpath):
                for filename in files:
                    path = os.path.join(dirpath, filename)
                    scan_eols_file(path)
        else:
            # It's a file, scan it directly
            scan_eols_file(rootpath)

def scan_eols_file(path):
    with open(path, "rb") as f:
        scan_eols_stream(f, path)

# Scan an input stream for the EOL types it has.
def scan_eols_stream(f, name):
    cr_count = 0
    lf_count = 0
    crlf_count = 0
    nul_count = 0
    byte_count = 0

    STATE_OTHER = 0
    STATE_CR = 1
    STATE_LF = 2

    CR_CHAR = b'\r'
    LF_CHAR = b'\n'

    state = STATE_OTHER

    b = f.read(1)
    while b:
        byte_count += 1
        if b == b'\0':
            nul_count += 1

        if state == STATE_OTHER:
            if b == CR_CHAR:
                state = STATE_CR
            elif b == LF_CHAR:
                state = STATE_LF
            else:
                state = STATE_OTHER
        elif state == STATE_CR:
            if b == CR_CHAR:
                # We had a bare CR
                cr_count += 1
                # and we remain in STATE_CR since we have another CR.
                state = STATE_CR
            elif b == LF_CHAR:
                # We have a CRLF
                crlf_count += 1
                # and we begin looking for another char since it's a full EOL.
                state = STATE_OTHER
            else:
                # We had a bare CR
                cr_count += 1
                # and we begin looking for next EOL since this char is non-EOL.
                state = STATE_OTHER
        elif state == STATE_LF:
            if b == CR_CHAR:
                # We have now seen LF CR. We consider this as separate EOLs.
                lf_count += 1
                # and we go to STATE_CR since we have a CR.
                state = STATE_CR
            elif b == LF_CHAR:
                # We have LF LF
                lf_count += 1
                # and we stay in STATE_LF
                state = STATE_LF
            else:
                # We had a bare LF with no CR/LF after it
                lf_count += 1
                # and we begin looking for next EOL since this char is non-EOL.
                state = STATE_OTHER
        else:
            raise Exception("invalid state")

        b = f.read(1)

    # Guess whether it's a text file (only if no NUL characters).
    if nul_count == 0:
        filetype = "text"
    else:
        filetype = "binary"

    sys.stdout.write("%6d %6d %6d  %6s  %s\n" % (
        lf_count, crlf_count, cr_count, filetype, name))


### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
