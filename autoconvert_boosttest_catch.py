#!/usr/bin/env python
#
# Automatically convert Boost Test Library tests to Catch tests.
#
# Colin D Bennett, 2016-02-29.

import argparse
import os
import re
import sys


def main():
    parser = argparse.ArgumentParser(
            description="Convert Boost Test code to Catch.")

    parser.add_argument("files", nargs="*", help="files to process")

    args = parser.parse_args()

    if args.files:
        for f in args.files:
            convert_path(f, f)
    else:
        sys.stdout.write(convert(sys.stdin.read()))


def convert_path(inpath, outpath):
    with open(inpath, "r") as infile:
        instr = infile.read()

    outstr = convert(instr)

    with open(outpath, "w") as outfile:
        outfile.write(outstr)


def convert(content):
    # Be careful to manually review changes,
    # possibly precedence with "!" use for ASSERT_FALSE etc.
    # may change meaning.  Some uses extra () but not all below.

    replacements = [
        [
            "// Boost Test Library headers",
            "// Catch headers"
        ],
        [
            "#include <boost/test/unit_test.hpp>",
            "#include <catch.hpp>"
        ],
        [
            r"\bBOOST_AUTO_TEST_CASE\((.*)\)",
            r'TEST_CASE("\1")'
        ],
        [
            r"\bBOOST_CHECK\( *([^!].*)\);",
            r"CHECK(\1);"
        ],
        [
            r"\bBOOST_CHECK\( *!(.*)\);",
            r"CHECK_FALSE(\1);",
        ],
        [
            r"\bBOOST_REQUIRE\( *([^!].*)\);",
            r"REQUIRE(\1);"
        ],
        [
            r"\bBOOST_REQUIRE\( *!(.*)\);",
            r"REQUIRE_FALSE(\1);",
        ],
        [
            r"\bBOOST_CHECK_EQUAL\(([^,]+), *(.*)\);",
            r"CHECK(\2 == \1);"
        ],
        [
            r"\bBOOST_REQUIRE_EQUAL\(([^,]+), *(.*)\);",
            r"REQUIRE(\2 == \1);"
        ],
        [
            r'\bBOOST_CHECK_MESSAGE\(*(.*), *"?(.*)"?\);',
            r"CHECK(\1);  // \2"
        ],
        [
            r'\bBOOST_REQUIRE_MESSAGE\(*(.*), *"?(.*)"?\);',
            r"REQUIRE(\1);  // \2"
        ],
        [
            r"\bBOOST_FAIL\((.*)\);",
            r"FAIL(\1);"
        ],
    ]

    for replacement in replacements:
        content = re.sub(replacement[0], replacement[1], content)

    return content


# Run the main program

if __name__ == "__main__":
    main()
