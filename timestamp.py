#!/usr/bin/env python3

"""Pipe text through this program to timestamp the lines."""

import sys
from datetime import datetime


def standalone_main() -> None:
    try:
        main(sys.argv[1:])
    except KeyboardInterrupt:
        raise SystemExit(130)  # Linux Ctrl+c exit code


def main(args) -> None:
    for line in sys.stdin:
        now = datetime.now().isoformat().replace("T", " ")
        line = line.rstrip("\n").rstrip("\r")
        sys.stdout.write(now)
        sys.stdout.write(" ")
        sys.stdout.write(line)
        sys.stdout.write("\n")
        sys.stdout.flush()


if __name__ == "__main__":
    standalone_main()
