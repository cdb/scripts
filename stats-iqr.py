#!/usr/bin/env python3
# Calculate stats with median and IQR

import statistics
import sys


def main():
    values = [float(line.strip()) for line in sys.stdin]

    # q0 = min(values)
    # q1, q2, q3 = statistics.quantiles(values)
    # q4 = max(values)
    (q0, q1, q2, q3, q4) = quartile(values)

    sys.stdout.write("Q0 (min)   : {}\n".format(q0))
    sys.stdout.write("Q1         : {}\n".format(q1))
    sys.stdout.write("Q2 (median): {}\n".format(q2))
    sys.stdout.write("Q3         : {}\n".format(q3))
    sys.stdout.write("Q4 (max)   : {}\n".format(q4))
    iqr = q3 - q1
    sys.stdout.write("IQR        : {}\n".format(iqr))
    sys.stdout.write("count      : {}\n".format(len(values)))
    outlier_distance = 1.5 * iqr
    outliers = len([x for x in values if x < q1 - outlier_distance or x > q3 + outlier_distance])
    sys.stdout.write("outliers   : {}\n".format(outliers))


def quartile(data):
    data.sort()
    half_list = int(len(data) // 2)
    q0 = data[0]
    q1 = statistics.median(data[:half_list])
    q2 = data[half_list]
    q3 = statistics.median(data[-half_list:])
    q4 = data[-1]
    return (q0, q1, q2, q3, q4)


if __name__ == "__main__":
    main()
