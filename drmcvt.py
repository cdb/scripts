#!/usr/bin/env python
# Convert DRM_MODE(...) to tabular form?
# CDB, 20 May 2013.

import re
import sys

content = sys.stdin.read()
  
sys.stdout.write(
    "%3s" % "VIC" +
    " %17s" % "Name" +
    " %6s" % "PixClk" +
    " %4s" % "VAct" +
    " %4s" % "VTot" +
    " %3s" % "VBl" +
    " %3s" % "VFP" +
    " %3s" % "VBP" +
    " %3s" % "VSL" +
    " %4s" % "HAct" +
    " %4s" % "HTot" +
    " %4s" % "HBl" +
    " %4s" % "HFP" +
    " %4s" % "HBP" +
    " %4s" % "HSL" +
    " %s" % "H" +
    " %s" % "V" +
    " %3s" % "Int" +
    "\n"
)

pat = (r"/\* (\d+) *- *(\S*)\s*\*/\s*" +  # (1) vic (2) desc: 1920x1080@60Hz
       "{\s*DRM_MODE\(" +            #      # Macro
       "\"([^\"]+)\",\s*" +      # nm   # (3) resolution name e.g. 1920x1080
       "\w+,\s*" +               # t    # ignore 'DRM_MODE_TYPE_DRIVER'
       "(\d+)\s*,\s*" +          # c    # (4) pixel clock e.g. 74250=74.25MHz
       "(\d+)\s*,\s*" +          # hd   # (5) H active
       "(\d+)\s*,\s*" +          # hss  # (6) H sync start
       "(\d+)\s*,\s*" +          # hse  # (7) H sync end
       "(\d+)\s*,\s*" +          # ht   # (8) H total
       "(\d+)\s*,\s*" +          # hsk  # (9) H skew
       "(\d+)\s*,\s*" +          # vd    (10) V active
       "(\d+)\s*,\s*" +          # vss   (11) V sync start
       "(\d+)\s*,\s*" +          # vse   (12) V sync end
       "(\d+)\s*,\s*" +          # vt    (13) V total
       "(\d+)\s*,\s*" +          # vs    (14) V scan (always 0)
       "([^)]+)\)"               # f     (15) flags
)


for match in re.finditer(pat, content, re.M):
    vic = int(match.group(1))
    name = match.group(2)
    nm = match.group(3)
    pixel_clock = int(match.group(4))
    h_active = int(match.group(5))
    h_sync_start = int(match.group(6))
    h_sync_end = int(match.group(7))
    h_total = int(match.group(8))
    h_skew = int(match.group(9))
    v_active = int(match.group(10))
    v_sync_start = int(match.group(11))
    v_sync_end = int(match.group(12))
    v_total = int(match.group(13))
    v_scan = int(match.group(14))  # always 0
    flags = match.group(15)

    h_fp = h_sync_start - h_active
    h_synclen = h_sync_end - h_sync_start
    h_bp = h_total - h_sync_end
    h_blanking = h_total - h_active

    v_fp = v_sync_start - v_active
    v_synclen = v_sync_end - v_sync_start
    v_bp = v_total - v_sync_end
    v_blanking = v_total - v_active

    if "NHSYNC" in flags:
        hsync_pol = "-"
    else:
        hsync_pol = "+"

    if "NVSYNC" in flags:
        vsync_pol = "-"
    else:
        vsync_pol = "+"

    # DBLCLK set only for exact doubles? not set for all with pixel replication
    if "DBLCLK" in flags:
        dblclk = "y"
    else:
        dblclk = "n"

    if "INTERLACE" in flags:
        interlaced = "y"
    else:
        interlaced = "n"

    sys.stdout.write(
        "%3d" % vic +
        " %17s" % name +
        " %6d" % pixel_clock +
        " %4d" % v_active +
        " %4d" % v_total +
        " %3d" % v_blanking +
        " %3d" % v_fp +
        " %3d" % v_bp +
        " %3d" % v_synclen +
        " %4d" % h_active +
        " %4d" % h_total +
        " %4d" % h_blanking +
        " %4d" % h_fp +
        " %4d" % h_bp +
        " %4d" % h_synclen +
        " %s" % hsync_pol +
        " %s" % vsync_pol +
        " %3s" % interlaced +
        "\n"
    )

# vim: set et ft=python ts=4 sw=4:
