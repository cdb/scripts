#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import errno
import logging
import os
import re
import subprocess
import tempfile
import time
import zipfile

from contextlib import contextmanager

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(levelname)s %(message)s')


def main():
    parser = argparse.ArgumentParser(
        description="""\
Create a device image with FAT filesystem and partition.
This requires Debian/Linux tools: kpartx, mount, parted, and mkfs.
This typically must be run with root permissions
because it needs to manage and mount loopback devices.""")

    parser.add_argument(
        "-o", "--output", required=True,
        help="specify output image file")

    parser.add_argument(
        "--compress", action="store_true",
        help="compress output disk image with ZIP")

    parser.add_argument(
        "--disk-size", default="4GB",
        help="disk size (e.g. 4GB)")

    parser.add_argument(
        "--input-zip", required=True,
        help="input ZIP file")

    parser.add_argument(
        "--quiet", "-q", action="store_true",
        help="quiet output")
    parser.add_argument(
        "--verbose", "-v", action="store_true",
        help="verbose output")

    args = parser.parse_args()

    if args.quiet:
        logging.getLogger().setLevel(logging.WARNING)

    if args.verbose:
        logging.getLogger().setLevel(logging.DEBUG)

    if not os.path.exists(args.input_zip):
        sys.stderr.write("input zip file not found\n")
        sys.exit(1)

    if args.compress:
        with tempfile.TemporaryDirectory() as imgdir:
            diskimage = os.path.join(imgdir, "disk.img")
            make_image(diskimage, args.disk_size, args.input_zip)
            create_zip(args.output, diskimage)
    else:
        diskimage = args.output
        make_image(diskimage, args.disk_size, args.input_zip)


def make_image(diskimage, disksize, inputzip):
    create_sparse_file(diskimage, disksize)
    create_partition_table(diskimage)
    with loopback_map_partition(diskimage) as partition_dev:
        mkfs(partition_dev, "vfat")
        with scoped_mount(partition_dev) as mountpoint:
            unzip(inputzip, mountpoint)


def create_sparse_file(filename, size_string):
    logging.debug("Create {0} with size {1}".format(
        filename, size_string))

    remove_if_exists(filename)
    subprocess.check_call([
        "truncate",
        "--size={0}".format(size_string),
        filename])


def remove_if_exists(filename):
    """Remove a file, but do nothing if it doesn't exist."""
    try:
        os.remove(filename)
    except OSError as e:
        if e.errno != errno.ENOENT:
            raise  # re-raise exception if not ENOENT


def create_partition_table(device):
    logging.debug("Partition " + device)
    subprocess.check_call(
        ["parted", device, "--script",
         "mklabel", "msdos",
         "mkpart", "p", "fat32", "0%", "100%"])


@contextmanager
def loopback_map_partition(diskimagefile):
    # $ sudo kpartx -va out
    # add map loop0p1 (253:0): 0 7809024 linear /dev/loop0 2048
    # $
    add_output = (subprocess.check_output(
        ["kpartx", "-va", diskimagefile])
        .decode("utf-8"))
    m = re.match(r"add map (\w+)", add_output)
    if not m:
        raise Exception("kpartx -va " + diskimagefile +
                        " failed. output is '" + add_output + "'")

    partition_device = "/dev/mapper/" + m.group(1)
    logging.debug("Partition in image file mapped to {0}".format(
        partition_device))

    try:
        yield partition_device
    finally:
        unmap_loop_partition(diskimagefile)


@contextmanager
def scoped_mount(filesystem):
    with tempfile.TemporaryDirectory("mnt") as mountpoint:
        logging.debug("Temporary mount point: {0}".format(
            mountpoint))

        mount(filesystem, mountpoint)
        try:
            yield mountpoint
        finally:
            umount(mountpoint)


def mount(filesystem, mountpoint):
    logging.debug("Mount {0} on {1}".format(
        filesystem, mountpoint))
    subprocess.check_call(["mount", filesystem, mountpoint])


def umount(arg):
    logging.debug("Umount {0}".format(arg))
    subprocess.check_call(["umount", arg])


def mkfs(target, fstype):
    logging.debug("Creating {0} filesystem on {1}".format(
        fstype, target))

    # We need to retry for a while if mkfs fails, because it seems
    # that the kpartx device mapping process has a delay before
    # it is ready.
    failures = 0
    while failures < 20:
        if subprocess.call(["mkfs", "-t", fstype, target]) == 0:
            logging.debug("Filesystem created after {0} "
                          "failed attempts".format(failures))
            return  # ok
        else:
            failures += 1
            time.sleep(0.1)

    raise Exception("failed mkfs -t {0} {1}".format(fstype, target))


def unmap_loop_partition(diskimagefile):
    logging.debug("Unmapping loop partition mappings for {0}".format(
        diskimagefile))

    # We need to retry for a while if mkfs fails, because it seems
    # that the kpartx device mapping process has a delay before
    # it is ready.
    failures = 0
    while failures < 20:
        if subprocess.call(["kpartx", "-vd", diskimagefile]) == 0:
            logging.debug("Cleaned up partition loop mapping "
                          "after {0} failures".format(failures))
            return  # ok
        else:
            failures += 1
            time.sleep(0.1)

    raise Exception("failed kpartx -vd {0}".format(diskimagefile))


def unzip(zipfilename, destdir):
    logging.debug("Extracting {0} to {1}".format(
        zipfilename, destdir))

    with zipfile.ZipFile(zipfilename, "r") as z:
        z.extractall(destdir)


def create_zip(zipfilename, inputfile):
    logging.debug("Creating ZIP archive {0} from {1}".format(
        zipfilename, inputfile))

    with zipfile.ZipFile(zipfilename, "w",
                         compression=zipfile.ZIP_DEFLATED) as z:
        z.write(inputfile, arcname=os.path.split(inputfile)[1])


if __name__ == "__main__":
    main()

# vim: set et ts=4 sw=4 tw=72:
