#!/bin/bash
#
# Prepare a project release by merging topic branches.
# Create an rc branch and then run this script, passing it a file
# listing the topic branches to merge.
#
# It includes the following features:
#
# * User can specify a file containing the branch names to merge
#
# * Merges the branches in the order they appear in the file
#
# * Uses automatic rerere index update, if merges succeed it will
#   proceed automatically.
#
# Colin Bennett, 27 Jan 2014.

# Function: exit_usage [REASON]
exit_usage() {
    if [ -n "$1" ]; then
        # Print the provided reason for usage printing.
        echo "$1"
        echo
    fi
    echo "Prepare a release branch by merging topic branches."
    echo "Usage: $0 --branches-from BRANCHES.txt"
    echo "    OR $0 BRANCH1 [BRANCH2...]"
    echo
    echo "Merge all branches listed in BRANCHES.txt or on the"
    echo "command line as BRANCH1, BRANCH2, etc."
    echo "into the current branch."
    echo
    exit 1
}

# Function: die REASON
die() {
    echo "Error: $1" >&2
    exit 1
}

require_clean_work_tree () {
    # Update the index
    git update-index -q --ignore-submodules --refresh
    err=0

    # Disallow unstaged changes in the working tree
    if ! git diff-files --quiet --ignore-submodules --
    then
        echo >&2 "cannot $1: you have unstaged changes."
        git diff-files --name-status -r --ignore-submodules -- >&2
        err=1
    fi

    # Disallow uncommitted changes in the index
    if ! git diff-index --cached --quiet HEAD --ignore-submodules --
    then
        echo >&2 "cannot $1: your index contains uncommitted changes."
        git diff-index --cached --name-status -r --ignore-submodules HEAD -- >&2
        err=1
    fi

    if [ $err = 1 ]
    then
        echo >&2 "Please commit or stash them."
        exit 1
    fi
}


#
# Main program
#

# Need an argument.
[ $# -gt 0 ] || exit_usage


if [ "$1" = "--branches-from" ]; then
    # The argument value specifies branch list file name.
    shift
    [ $# = 1 ] || exit_usage "need branch file name"
    BRANCHFILE="$1"
    shift
    BRANCHES=( $(cat "$BRANCHFILE") )
else
    # Remaining arguments are branch names.
    BRANCHES=( $* )
fi

# Require a clean work tree.
require_clean_work_tree "multi merge"
# Or, the following simple version:
#if [ -n "$(git status -s)" ]; then
#    die "need a clean work tree before multi merge"
#fi

nmerged=0
nconflicts=0
nbranches=${#BRANCHES[@]}

# Read and iterate over words from $BRANCHFILE.
for branch in ${BRANCHES[@]}; do
    nmerged=$(( $nmerged + 1 ))
    echo
    echo "==== #${nmerged}/${nbranches} merge $branch ===="
    git merge \
        --no-ff \
        -q \
        --rerere-autoupdate \
        -e \
        -m "$( echo -e "Merge ${branch}\n\n" ; git log ..${branch} )" \
        "$branch"
    if [ $? -eq 0 ]; then
        echo "---- OK: no conflicts"
    else
        if [ $( git ls-files -u | wc -l ) = 0 ]; then
            if git commit --no-edit; then
                echo "---- OK: conflicts automatically resolved"
                nconflicts=$(( $nconflicts + 1 ))
            else
                echo "---- FAIL: conflicts remain"
                die "failed to commit merge"
            fi
        else
            echo "---- FAIL: conflicts remain"
            die "failed to merge"
        fi
    fi

    require_clean_work_tree "after #${nmerged}/${nbranches} merge ${branch}"
done

echo
echo ".... DONE: merged ${nmerged} branches,"
echo "                  ${nconflicts} of which had conflicts"
echo


# vim: set et ts=4 sw=4 ft=sh:
