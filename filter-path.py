#!/usr/bin/env python
# -*- coding: utf-8 -*-

import argparse
import os
import re
import subprocess
import sys


def main():
    parser = argparse.ArgumentParser(
            description="""
Run a program without a particular system PATH element.

Example:

    filter-path.py --exclude 'srecord' make all
""",
            formatter_class=argparse.RawDescriptionHelpFormatter)

    parser.add_argument(
            "--verbose", "-v", action="store_true",
            help="verbose output")
    parser.add_argument(
            "--use-modified", action="store_true",
            help="use the modified path to find 'command' itself, "
                 "in addition to the normal behavior of passing "
                 "the modified path to command once it is started")
    parser.add_argument(
            "--exclude", "-e", action="append", default=[],
            help="exclude path elements matching this regex")
    parser.add_argument(
            "command", nargs="+",
            help="command to execute")

    args = parser.parse_args()

    oldpathstr = os.environ.get("PATH", "")
    oldpath = oldpathstr.split(os.pathsep)
    newpath = [
            element for element in oldpath
            if not any(re.search(excludepat, element)
                       for excludepat in args.exclude)
    ]
    newpathstr = os.pathsep.join(newpath)
    newenv = os.environ.copy()
    newenv["PATH"] = newpathstr

    if args.use_modified:
        # Note: If we want to use the new search PATH for finding
        # the program we are asked to execute,
        # we must modify the actual environment variable PATH
        # in the Python process rather than the usual best practice
        # of making a copy which is modified and passed to Popen as
        # the 'env' argument.  This is because at least on Windows
        # the Popen() function is calling execvp() which uses the
        # parent process PATH to search.
        os.environ["PATH"] = newpathstr

    if args.verbose:
        sys.stdout.write("Original path: {}\n\n".format(oldpathstr))
        sys.stdout.write("Modified path: {}\n\n".format(newpathstr))
        sys.stdout.write("Calling program: {}\n\n".format(args.command))

    return subprocess.call(args.command, env=newenv)


# Run the main program
if __name__ == "__main__":
    main()
