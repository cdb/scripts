#!/usr/bin/env python3

import secrets

def main():
    print(makepass(length=8))

def makepass(length):
    uc = rand_lower_letter().upper()
    punc = '.'
    num = rand_digit()
    s = uc + repstr(rand_lower_letter, length - 3) + punc + num
    return s 

def char_range(a, b):
    return [chr(c) for c in range(ord(a), ord(b) + 1)]

def repstr(f, n):
    return ''.join(f() for i in range(n))

def rand_digit():
    return choice(char_range('0', '9'))

def rand_lower_letter():
    return choice(char_range('a', 'z'))

def choice(items):
    return secrets.choice(items)

if __name__ == '__main__':
    main()
