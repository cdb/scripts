#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Launchmeister
#
# By Colin D Bennett, 14 April 2014.
#
# Start up a list of programs automatically.
# Optional delay at the beginning and between programs.
# Optional interactive cancel through GUI.
# Can be configured through command-line options only, or using a
# YAML configuration file for more control.
#
# Requirements:
# - Python 3.4
# - PyYAML (pip install PyYAML)
# - PySide (download and run installer)

import argparse
import logging
import os
import re
import subprocess
import sys

# PyYAML
import yaml

# PySide (Qt 4.8)
from PySide.QtCore import QTimer
from PySide.QtGui import QApplication
from PySide.QtGui import QDialog
from PySide.QtGui import QIcon
from PySide.QtGui import QLabel
from PySide.QtGui import QListView
from PySide.QtGui import QPushButton
from PySide.QtGui import QStandardItem
from PySide.QtGui import QStandardItemModel
from PySide.QtGui import QVBoxLayout
import PySide


def main():
    # Setup logging.
    logfile = os.path.join(os.path.expanduser("~"), "launchmeister.log")
    logging.basicConfig(
            filename=logfile,
            level=logging.INFO,
            format='%(asctime)s %(levelname)s %(message)s')
    # Also log to stderr.
    logging.getLogger().addHandler(logging.StreamHandler())
    # Log the startup message.
    logging.info("========================================================")
    logging.info("Started")
    logging.info("========================================================")

    try:
        # Setup command line parser
        parser = argparse.ArgumentParser(
                description="""
Scripted program launching tool.

Configuration file is a YAML file. Example:

########################################
initial_delay: 10
delay_between: 5
commands:
  - name: Paint
    command: mspaint.exe
  - name: Notepad
    command: notepad.exe
########################################
                """,
                formatter_class=argparse.RawDescriptionHelpFormatter)

        parser.add_argument(
                "--quiet", "-q", action="store_true",
                help="quiet output")
        parser.add_argument(
                "--verbose", "-v", action="store_true",
                help="verbose output")
        parser.add_argument(
                "--initial-delay", type=int, default=0,
                help="delay before starting anything (s)")
        parser.add_argument(
                "--delay-between", type=int, default=0,
                help="delay between commands (s)")
        parser.add_argument(
                "--config-file",
                help="configuration file (yaml)")
        parser.add_argument(
                "commands", nargs="*",
                help="commands to execute")

        args = parser.parse_args()

        if args.quiet:
            logging.getLogger().setLevel(logging.WARNING)

        if args.verbose:
            logging.getLogger().setLevel(logging.DEBUG)

        commands = []

        # Process configuration file
        if args.config_file:
            import yaml
            with open(args.config_file, "r") as f:
                cfg = yaml.load(f)

            if "initial_delay" in cfg:
                args.initial_delay = cfg["initial_delay"]

            if "delay_between" in cfg:
                args.delay_between = cfg["delay_between"]

            if "commands" in cfg:
                for ycommand in cfg["commands"]:
                    if "command" in ycommand:
                        command = ycommand["command"]
                    else:
                        logging.error("Missing 'command' in config")

                    if "name" in ycommand:
                        name = ycommand["name"]
                    else:
                        # Default to the command itself
                        name = command

                    commands.append(Command(name, command))

        # Add any commands given on the command line.
        for c in args.commands:
            commands.append(Command(c, c))

        # Launch the commands
        Launcher().launch(
                commands,
                initial_delay=args.initial_delay,
                delay_between=args.delay_between)

        logging.info("Finished")

    except Exception:
        logging.error("Terminated by uncaught exception", exc_info=True)
        raise


class Command(object):
    def __init__(self, title, command):
        self.title = title
        self.command = command


class Launcher(object):
    def launch(self, commands, initial_delay=0, delay_between=1):
        self.commands = commands
        self.delay_between = delay_between

        app = QApplication(sys.argv)

        self.dialog = QDialog()

        cancel_button = QPushButton("Cancel")
        #cancel_button.setDefault(True)
        cancel_button.clicked.connect(self.cancel)

        self.status_label = QLabel()

        self.cmdlistview = QListView(self.dialog)
        self.cmdlistmodel = QStandardItemModel()
        for c in self.commands:
            item = QStandardItem()
            item.setText("[PENDING] " + c.title)
            item.setData(c)
            #item.setIcon(QIcon.fromTheme("edit-cut"))
            self.cmdlistmodel.appendRow(item)

        self.cmdlistview.setModel(self.cmdlistmodel)

        layout = QVBoxLayout()
        layout.addWidget(self.status_label)
        layout.addWidget(self.cmdlistview)
        layout.addWidget(cancel_button)

        self.dialog.setWindowTitle("Launchmeister")
        self.dialog.setLayout(layout)
        sz = 384
        self.dialog.resize(sz, sz / 1.618)

        self.initial_delay_timer = QTimer()
        self.initial_delay_timer.setInterval(0)
        self.initial_delay_timer.setSingleShot(True)
        self.initial_delay_remain = initial_delay
        self.initial_delay_timer.timeout.connect(self.initial_delay_tick)

        self.next_delay_timer = QTimer()
        self.next_delay_timer.timeout.connect(self.next_delay_tick)

        self.next_cmd_index = 0
        logging.info("Launcher waiting for initial delay")
        self.initial_delay_timer.start()

        self.dialog.exec_()

    def start_next(self):
        self.initial_delay_timer.stop()
        self.next_delay_timer.stop()

        i = self.next_cmd_index
        self.next_cmd_index += 1

        if i < self.cmdlistmodel.rowCount():
            item = self.cmdlistmodel.item(i)
            cmd = item.data()
            # Run the program.
            # If we don't depend on its output, Python will
            # run it in the background.
            logging.info("Executing %s: %s" % (cmd.title, cmd.command))
            try:
                child = subprocess.Popen(cmd.command)
                logging.debug("Command %s launched" % cmd.title)
                item.setText("[DONE] " + cmd.title)
            except Exception:
                item.setText("[FAILED] " + cmd.title)
                logging.error("Failed to execute %s" % cmd.command,
                        exc_info=True)

            if self.next_cmd_index < self.cmdlistmodel.rowCount():
                # There is another command to run.
                self.next_delay_timer.setInterval(0)
                self.next_delay_timer.setSingleShot(True)
                self.next_delay_remain = self.delay_between
                self.next_delay_timer.start()
            else:
                # This was the last command. Quit.
                logging.info("Launcher finished; launched last command")
                self.dialog.close()
        else:
            # No more commands
            logging.info("Launcher finished; no commands")
            self.dialog.close()

    def initial_delay_tick(self):
        if self.initial_delay_remain > 0:
            s = "Beginning after "
            s += "%d" % self.initial_delay_remain
            s += " sec"
            self.status_label.setText(s)
            self.initial_delay_remain -= 1
            self.initial_delay_timer.setInterval(1000)
            self.initial_delay_timer.start()
        else:
            self.start_next()

    def next_delay_tick(self):
        if self.next_delay_remain > 0:
            s = "Starting next after "
            s += "%d" % self.next_delay_remain
            s += " sec"
            self.status_label.setText(s)
            self.next_delay_remain -= 1
            self.next_delay_timer.setInterval(1000)
            self.next_delay_timer.start()
        else:
            self.next_delay_timer.stop()
            self.start_next()

    def cancel(self):
        logging.warning("Launch canceled by user")
        self.next_delay_timer.stop()
        self.initial_delay_timer.stop()
        self.dialog.close()


### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
