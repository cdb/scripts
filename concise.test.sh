#!/bin/sh

script="$(dirname "$0")/concise.sh"
export CONCISE_FAILURE_LINES=3
export CONCISE_PATTERN="^>>>"

testpoints=0
passed=0
failed=0

check() {
    desc="$1"
    shift
    testpoints=$(($testpoints + 1))
    if "$@"; then
        echo "ok $testpoints $desc"
        passed=$(($passed +  1))
    else
        echo "not ok $testpoints $desc"
        failed=$(($failed +  1))
    fi
}

tapstart() {
    echo "TAP version 13"
}

tapfinish() {
    # Write the plan at the end of the test output.
    echo "1..${testpoints}"

    # And summarize the results.
    # This format is used by 'tape' http://github.com/substack/tape.
    # The comments are additional comments that are not required
    # by the TAP protocol itself.
    echo "# tests ${testpoints}"
    echo "# pass  ${passed}"
    if [ $failed -eq 0 ]; then
        echo
        echo "# ok"
        exit 0
    else
        echo "# fail  ${failed}"
        exit 1
    fi
}

outdir=$(mktemp -d --tmpdir test-concise.XXXXXX)

tapstart

# Test successful script, only the high-level lines printed.
outfile="${outdir}/1-actual"
expectedout="${outdir}/1-expected"
"$script" sh -c "echo '>>> Start'; seq -f 'Line %.0f' 500; echo '>>> End'" >"$outfile"
check "success" [ $? -eq 0 ]
cat >> "$expectedout" <<END
>>> Start
>>> End
END
check "brief output, env var settings" diff -u "$expectedout" "$outfile"

# Test failing script, final $CONCISE_FAILURE_LINES will be printed.
outfile="${outdir}/2-actual"
expectedout="${outdir}/2-expected"
"$script" sh -c "echo '>>> Start'; seq -f 'Line %.0f' 500; echo '>>> End'; exit 1" >"$outfile"
check "error code" [ $? -eq 1 ]
cat >> "$expectedout" <<END
>>> Start
>>> End

----

Error:

Line 499
Line 500
>>> End
END
check "failure output, env var settings" diff -u "$expectedout" "$outfile"

# Test successful script, only the high-level lines printed.
outfile="${outdir}/3-actual"
expectedout="${outdir}/3-expected"
"$script" --failure-lines=5 --pattern=DUDE \
    sh -c "echo 'Start DUDE.'; seq -f 'Line %.0f' 500; echo 'End DUDE.'" \
    >"$outfile"
check "success" [ $? -eq 0 ]
cat >> "$expectedout" <<END
Start DUDE.
End DUDE.
END
check "brief output, env var settings" diff -u "$expectedout" "$outfile"

# Test failing script.
outfile="${outdir}/4-actual"
expectedout="${outdir}/4-expected"
"$script" "--pattern=DU*DE" --failure-lines=5 -- \
    sh -c "echo 'Start DUUDE.'; seq -f 'Line %.0f' 500; echo 'End DUUUDE.'; exit 1" \
    >"$outfile"
check "error code" [ $? -eq 1 ]
cat >> "$expectedout" <<END
Start DUUDE.
End DUUUDE.

----

Error:

Line 497
Line 498
Line 499
Line 500
End DUUUDE.
END
check "failure output, env var settings" diff -u "$expectedout" "$outfile"

# Suppress all lines on success.
outfile="${outdir}/5-actual"
expectedout="${outdir}/5-expected"
"$script" --failure-lines=5 --pattern= \
    sh -c "echo 'Start DUDE.'; echo; echo; seq -f 'Line %.0f' 500; echo 'End DUDE.'" \
    >"$outfile"
check "success" [ $? -eq 0 ]
cat >> "$expectedout" <<END
END
check "no output on success" diff -u "$expectedout" "$outfile"

# Suppress all lines except failure.
outfile="${outdir}/6-actual"
expectedout="${outdir}/6-expected"
"$script" --failure-lines=5 --pattern= \
    sh -c "echo 'Start DUDE.'; echo; echo; seq -f 'Line %.0f' 500; echo 'End DUDE.'; exit 1" \
    >"$outfile"
check "success" [ $? -eq 1 ]
cat >> "$expectedout" <<END

----

Error:

Line 497
Line 498
Line 499
Line 500
End DUDE.
END
check "only error output" diff -u "$expectedout" "$outfile"

# Suppress all lines except failure.
outfile="${outdir}/7-actual"
expectedout="${outdir}/7-expected"
CONCISE_PATTERN="" "$script" --failure-lines=5 \
    sh -c "echo '>>> Start DUDE.'; echo; echo; seq -f 'Line %.0f' 500; echo '>>> End DUDE.'; exit 1" \
    >"$outfile"
check "success" [ $? -eq 1 ]
cat >> "$expectedout" <<END

----

Error:

Line 497
Line 498
Line 499
Line 500
>>> End DUDE.
END
check "only error output via env var pattern" diff -u "$expectedout" "$outfile"

rm -r "$outdir"
tapfinish
