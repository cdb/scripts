#!/bin/sh

usage() {
    echo "Usage: $0 LOGFILE"
    echo "Read LOGFILE, which consists of docker-compose logs."
    echo "Write each container's log file to a separate log file on disk."
    exit 0
}

LOGFILE="$1"
[ -n "$LOGFILE" ] || usage

containers=$(cut -d ' ' -f 1 "$LOGFILE" | sort | uniq)
for c in $containers; do
    perl -ne '/^'$c'\s*\|\s*(.*)/ and print "$1\n"' "$LOGFILE" > "$c.log"
done
