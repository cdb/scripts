#!/usr/bin/env python3
# Convert a Spring Boot configuration from application.properties file format to
# environment variables format. This means replacing periods and hyphens with
# underscores, and also capitalizes it for consistency with standard
# environment variable conventions.

import re
import sys


def main():
    for line in sys.stdin:
        sys.stdout.write(properties_line_to_env_line(line))


def properties_line_to_env_line(line):
    return re.sub(
        r"^([^=]+)=(.*)$", lambda m: to_env_name(m.group(1)) + "=" + m.group(2), line
    )


def to_env_name(s):
    return s.upper().replace(".", "_").replace("-", "_")


if __name__ == "__main__":
    main()
