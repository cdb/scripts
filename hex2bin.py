#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Program to convert a file of HEX bytes into binary form.
#
# By Colin D Bennett, 13 May 2013.

import re
import argparse
import os
import sys

def main():
    parser = argparse.ArgumentParser(
            description="Convert a raw HEX file to binary.")

    parser.add_argument("-f", "--force", help="overwrite existing files",
            action="store_true")
    parser.add_argument("--relax", help="relaxed parsing, skip non-hex-byte-words",
            action="store_true")
    parser.add_argument("input", nargs="+", help="specify input file")
    parser.add_argument("-o", "--output", help="specify output file")

    args = parser.parse_args()

    for infile in args.input:
        if args.output:
            if len(args.input) > 1:
                sys.stderr.write("Can't handle --output with multiple input files")
                sys.exit(1)  # Exit with error code
            outfile = args.output
        else:
            # Output file unspecified, derive a name from input name.
            root, ext = os.path.splitext(infile)
            # Change the extension to .bin.
            outfile = root + ".bin"

        if os.path.exists(outfile) and not args.force:
            sys.stderr.write("Output file '" + outfile + "' already exists: "
                    + "specify --force to overwrite")
            sys.exit(1)  # Exit with error code

        unhexify_file(infile, outfile, relaxed=args.relax)

# Convert infile from hex to outfile in raw binary.
def unhexify_file(infile, outfile, relaxed):
    with open(infile, "r") as infile:
        with open(outfile, "wb") as outfile:
            for line in infile:
                outfile.write(unhexify_str(line, relaxed=relaxed))

# Return a 'bytes' object containing the raw bytes parsed from 's'.
def unhexify_str(s, relaxed):
    # We're generous with the pattern so invalid hex values
    # such as containing letters after F or too many digits
    # will be caught and cause and error, not silently ignored.
    if relaxed:
        tokenpat = r"\b(?:0x)?([0-9A-Fa-f]{2})\b"
    else:
        tokenpat = r"(?:0x)?([0-9A-Za-z]{2})"

    # Convert the hex values in s matched by tokenpat into a sequence
    # of bytes.
    # In Python 2 we need to use bytearray since bytes([3,7])=='[3, 7]'
    # but in Python 3 bytes([3,7])=b'\x03\x07'.
    return bytes(bytearray(
            [
                unhexify_byte(hex_token.group(1))
                    for hex_token in re.finditer(tokenpat, s)
            ]))

# Return the value of the byte in 's' as an int type.
def unhexify_byte(s):
    n = int(s, 16)
    if 0 <= n and n <= 255:
        return n
    else:
        raise ValueError("value %s is not a valid HEX byte" % s)


### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
