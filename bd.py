#!/usr/bin/python3

import os
import subprocess
import sys


def main():
    result = try_build_py()
    if not result.resolved:
        result = try_cmake()
    if not result.resolved:
        result = Result.make_failed(
                "Sorry, don't know how to build this project")

    exit_result(result)


def try_build_py():
    for d in walk_directories_to_root(os.getcwd()):
        script = os.path.join(d, "build.py")
        if os.path.exists(script):
            return run_python(script, sys.argv[1:])
    return Result.make_pending()


def try_cmake():
    for d in walk_directories_to_root(os.getcwd()):
        script = os.path.join(d, "CMakeLists.txt")
        if os.path.exists(script):
            return run_cmake(d, sys.argv[1:])
    return Result.make_pending()


def walk_directories_to_root(path):
    """Generator function yielding each directory up to the root."""
    at_top = False
    while not at_top:
        yield path
        parent_path = os.path.dirname(path)
        if parent_path == path:
            at_top = True
        else:
            path = parent_path


def run_python(script, args):
    cmd = ["python", script] + args
    rc = subprocess.call(cmd)
    if rc == 0:
        return Result.make_success()
    else:
        return Result.make_failed(
                "python command failed: " + " ".join(cmd))


def run_cmake(source_dir, args):
    config_args = [x for x in args if x.startswith("-D")]
    build_args = [x for x in args if not x.startswith("-D")]

    build_dir_name = os.getenv("BUILD_DIR_NAME", "build")
    build_dir = os.path.join(source_dir, build_dir_name)
    if not os.path.exists(build_dir):
        os.mkdir(build_dir)
    cmake_cmd = ["cmake", "-G", "Ninja", source_dir] + config_args
    rc = subprocess.call(cmake_cmd, cwd=build_dir)
    if rc != 0:
        return Result.make_failed(
                "cmake configure failed: " + " ".join(cmake_cmd))
    build_cmd = ["cmake", "--build", build_dir, "--"] + build_args
    rc = subprocess.call(build_cmd)
    if rc != 0:
        return Result.make_failed(
                message="cmake build failed: " + " ".join(build_cmd))

    return Result.make_success()


def exit_result(result):
    if result.message:
        sys.stdout.write(result.message + "\n")

    if result.success:
        sys.exit(0)
    else:
        sys.exit(1)


class Result(object):
    def __init__(self, resolved=False, success=False, message=None):
        self.resolved = resolved
        self.success = success
        self.message = message

    @staticmethod
    def make_success():
        return Result(resolved=True, success=True)

    @staticmethod
    def make_failed(message=None):
        return Result(resolved=True, success=False, message=message)

    @staticmethod
    def make_pending():
        return Result(resolved=False)


if __name__ == "__main__":
    main()
