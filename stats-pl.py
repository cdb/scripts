#!/usr/bin/env python3
# Calculate stats with median and IQR

from typing import Iterable
import polars as pl
import sys


def main() -> None:
    do_stats(sys.stdin)


def do_stats(input: Iterable[str]) -> None:
    values = [float(line.strip()) for line in input]
    series = pl.Series(values)

    sys.stdout.write("count      : {}\n".format(len(values)))
    sys.stdout.write("p0 (min)   : {}\n".format(series.min()))
    sys.stdout.write(
        "p1         : {}\n".format(series.quantile(0.01, interpolation="lower"))
    )
    sys.stdout.write(
        "p5         : {}\n".format(series.quantile(0.05, interpolation="lower"))
    )
    sys.stdout.write(
        "p10        : {}\n".format(series.quantile(0.10, interpolation="lower"))
    )
    sys.stdout.write(
        "p25        : {}\n".format(series.quantile(0.25, interpolation="lower"))
    )
    sys.stdout.write(
        "p50(median): {}\n".format(series.quantile(0.50, interpolation="lower"))
    )
    sys.stdout.write(
        "p75        : {}\n".format(series.quantile(0.75, interpolation="lower"))
    )
    sys.stdout.write(
        "p90        : {}\n".format(series.quantile(0.90, interpolation="lower"))
    )
    sys.stdout.write(
        "p95        : {}\n".format(series.quantile(0.95, interpolation="lower"))
    )
    p99 = series.quantile(0.99, interpolation="lower")
    p999 = series.quantile(0.999, interpolation="lower")
    p9999 = series.quantile(0.9999, interpolation="lower")
    sys.stdout.write("p99        : {}\n".format(p99))
    sys.stdout.write("p99.9      : {}\n".format(p999))
    sys.stdout.write("p99.99     : {}\n".format(p9999))
    sys.stdout.write("p100 (max) : {}\n".format(series.max()))

    q1 = series.quantile(0.25, interpolation="lower")
    q3 = series.quantile(0.75, interpolation="lower")
    iqr = q3 - q1
    sys.stdout.write("IQR Q3-Q1  : {}\n".format(iqr))
    outlier_distance = 1.5 * iqr
    outliers = len(
        [x for x in values if x < q1 - outlier_distance or x > q3 + outlier_distance]
    )
    sys.stdout.write("outliers 1.5IQR  : {}\n".format(outliers))
    sys.stdout.write(
        "outliers >p99    : {}\n".format(len([True for x in values if x > p99]))
    )
    sys.stdout.write(
        "outliers >p99.9  : {}\n".format(len([True for x in values if x > p999]))
    )
    sys.stdout.write(
        "outliers >p99.99 : {}\n".format(len([True for x in values if x > p9999]))
    )


def test_stats() -> None:
    do_stats(
        [
            "10",
            "20",
            "30",
            "32",
            "33",
            "35",
            "38",
            "38.5",
            "33.3",
            "32.01",
            "100",
            "30.5",
            "99",
        ]
    )


if __name__ == "__main__":
    main()
