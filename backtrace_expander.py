#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# backtrace_expander.py
#
# If you get UnicodeEncodeError
#
#   UnicodeEncodeError: 'charmap' codec can't encode character '\xa4' in
#   position 0: character maps to <undefined>
#
# because the text has some non-ASCII
# characters and Windows/Python are not playing nicely with character
# encodings, you might have to run the script with
#
#   PYTHONIOENCODING=utf-8:surrogateescape python backtrace_expander.py
#
# By Colin D Bennett, 5 February 2015.
#

import argparse
import io
import logging
import os
import re
import subprocess
import sys

def main():
    # Setup logging.
    logging.basicConfig(
            level=logging.INFO,
            format='%(asctime)s %(levelname)s %(message)s')

    try:
        # Setup command line parser
        parser = argparse.ArgumentParser(
                description="""
Expand backtraces by replacing code addresses with file/line numbers.
                """,
                formatter_class=argparse.RawDescriptionHelpFormatter)

        parser.add_argument(
                "--quiet", "-q", action="store_true",
                help="quiet output")
        parser.add_argument(
                "--verbose", "-v", action="store_true",
                help="verbose output")
        parser.add_argument(
                "--addr2line-program", default="addr2line",
                help="addr2line program to use")
        parser.add_argument(
                "--addr2line-options", default="--addresses --basenames --demangle --functions --pretty-print",
                help="set addr2line formatting options")
        parser.add_argument(
                "--exe", "-e", required=True,
                help="the program executable file (ELF)")
        parser.add_argument(
                "files", nargs="*",
                help="files to filter")

        args = parser.parse_args()

        if args.quiet:
            logging.getLogger().setLevel(logging.WARNING)

        if args.verbose:
            logging.getLogger().setLevel(logging.DEBUG)

        addr2line = Addr2line(
                args.exe,
                args.addr2line_program,
                args.addr2line_options.split())
        processor = Expander(addr2line)

        if args.files:
            for filename in args.files:
                if filename == "-":
                    processor.process(sys.stdin, sys.stdout)
                else:
                    with io.open(filename, "r", newline='') as f:
                        processor.process(f, sys.stdout)
        else:
            processor.process(sys.stdin, sys.stdout)

    except Exception:
        logging.error("Terminated by uncaught exception", exc_info=True)
        raise

class Addr2line(object):
    def __init__(self, exe, program, options):
        self.exe = exe
        self.program = program
        self.options = options

class Expander(object):
    def __init__(self, addr2line):
        self._addr2line = addr2line
        self._btfilt = Backtrace_Filter(addr2line)

    def process(self, f, outstream):
        multiline_backtrace = None

        for rawline in f:
            line = universalize_newline(rawline)
            # We look for two kinds of backtraces: inline and multi-line.
            #
            # (1) inline::
            #
            #         Backtrace: 4003C4D6 4003C4D6 4003C4D6
            #
            # (2) multi-line::
            #
            #         *** Backtrace:
            #         4003C4D6
            #         400F2B34
            #         4006B96C
            #         4006B3CE
            #         40107A9C
            #         401075B4
            #         4006F5B8
            #         4006F6DC
            #         400A1B3C
            #         ***

            line_processed = False

            if multiline_backtrace is not None:
                btline = re.search(r"^\s*([0-9a-fA-F]{8})\s*$", line)
                if btline:
                    # Add this address
                    multiline_backtrace += " " + btline.group(1)
                    line_processed = True
                else:
                    end_multiline_backtrace = re.search(r"^\*\*\*\s*$", line)
                    if end_multiline_backtrace:
                        self.expand_backtrace(multiline_backtrace, outstream)
                        multiline_backtrace = None
                        line_processed = True
                    else:
                        # Didn't match, consider the multiline backtrace done.
                        self.expand_backtrace(multiline_backtrace, outstream)
                        multiline_backtrace = None
                        # Don't mark the line as processed; we will consider
                        # it further below.

            if not line_processed:
                start_multiline_backtrace = re.search(r"^\*\*\* Backtrace:\s*$", line)
                if start_multiline_backtrace:
                    multiline_backtrace = ""
                    line_processed = True
                else:
                    inline_backtrace_match_1 = re.search(r"Backtrace\(([^)]+)\)", line)
                    if inline_backtrace_match_1:
                        outstream.write(line)
                        self.expand_backtrace(inline_backtrace_match_1.group(1), outstream)
                        line_processed = True
                    else:
                        inline_backtrace_match_2 = re.search(r"Backtrace:((?:\s*[0-9a-fA-F]{8})+)", line)
                        if inline_backtrace_match_2:
                            outstream.write(line)
                            self.expand_backtrace(inline_backtrace_match_2.group(1), outstream)
                            line_processed = True

            # Write any unprocessed lines out literally.
            if not line_processed:
                outstream.write(line)

        # After reading all lines, complete any unfinished work.
        if multiline_backtrace:
            self.expand_backtrace(multiline_backtrace, outstream)
            multiline_backtrace = None

    def expand_backtrace(self, backtrace_str, outstream):
        bt = Backtrace(backtrace_str)
        bt_expanded = self._btfilt.expand_backtrace(bt)
        outstream.write(
                "\n" +
                "--- Backtrace: ---\n" +
                bt_expanded +
                "------------------\n" +
                "\n"
        )

class Backtrace(object):
    def __init__(self, s):
        """
        Construct a backtrace object from the whitespace-separated
        address list in ``s``.
        """

        self._addrs = s.split()
        self._str = ' '.join(self._addrs)

    def str(self):
        return self._str

    def addrs(self):
        return self._addrs

    def __str__(self):
        return self.str()

    def __hash__(self):
        return hash(self.str())

    def __eq__(self, other):
        return self.str() == other.str()



class Backtrace_Filter(object):
    def __init__(self, addr2line):
        self.addr2line = addr2line

    def expand_backtrace(self, backtrace):
        cmd = [ self.addr2line.program ]
        cmd += self.addr2line.options
        cmd.append("--exe=" + self.addr2line.exe)
        cmd += backtrace.addrs()
        logging.debug("Calling addr2line as {}".format(cmd))
        return subprocess.check_output(cmd, universal_newlines=True)


def universalize_newline(line):
    return re.sub("\r\n", "\n", line)


### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
