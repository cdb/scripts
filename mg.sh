#!/usr/bin/env sh
# Multi-Git wrapper - run Git commands across multiple repositories
# Requirements:
# - parallel: GNU parallel
# - git: Git
# - rg: RipGrep from BurntSushi, needed for PCRE regexes

JOBS=4

main() {
    while [ "$#" -gt 0 ]; do
        case "$1" in
            current-branch)
                parallel -k -a .repos -j $JOBS $OPTIONS --tag git -C {} rev-parse --abbrev-ref HEAD
                break
                ;;
            git)
                shift
                parallel -k -a .repos -j $JOBS $OPTIONS --tag git -C {} "$@"
                break
                ;;
            outdated)
                # Show where the checked-out branch is out of date with the remote
                parallel -k -a .repos -j $JOBS $OPTIONS --tag "git -C {} status | rg -P \"Your branch is (?"'!'"up to date)\""
                break
                ;;
            pull)
                parallel -k -a .repos -j $JOBS $OPTIONS --tag git -C {} pull --ff-only --rebase=false
                break
                ;;
            st)
                parallel -k -a .repos -j $JOBS $OPTIONS --tag git -C {} status -s
                break
                ;;
            help|-h|--help)
                echo "Usage: $0 [-d|--delay DELAY_S] [-j|--jobs N] COMMAND"
                echo "  COMMAND:"
                echo "    current-branch    show the current branch for each"
                echo "    git ARGS..        run git with ARGS"
                echo "    outdated          show where the checked-out branch is out of date with the remote"
                echo "    pull              git pull"
                echo "    st                git status"
                exit 1
                ;;
            -d|--delay)
                shift
                OPTIONS="$OPTIONS --delay $1"
                ;;
            -j|--jobs)
                shift
                JOBS=$1
                ;;
            -*)
                echo "ERROR: Unknown option $1"
                exit 1
                ;;
            *)
                echo "ERROR: Unknown command $1"
                exit 1
                ;;
        esac
        shift
    done
}

main "$@"
