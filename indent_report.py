#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Produce a report on the indentation style (tabs vs. spaces) in a tree of
# files.
# Colin D Bennett, 2014-04-03.

import argparse
import os
import re
from sys import stdout
from sys import stdin


indent_pattern = re.compile(r"^(\s+)\S")

def main():
    parser = argparse.ArgumentParser(
            description="Produce a report on indentation style.")

    parser.add_argument("paths", nargs="*", help="specify file/directory paths")
    parser.add_argument("--paths-from-stdin", action="store_true", help="read file paths on lines from stdin")
    parser.add_argument("--quiet", action="store_true", help="quiet")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose output")

    args = parser.parse_args()

    paths = []
    if args.paths_from_stdin:
        paths += [line.rstrip('\n') for line in stdin]
    
    paths += args.paths

    stats = WhitespaceStats()
    for path in paths:
        stats.process(path)
    stats.print_stats()


class WhitespaceStats(object):
    def __init__(self):
        self.space_files = 0
        self.tab_files = 0
        self.mixed_files = 0
        self.none_files = 0
        self.space_lines = 0
        self.tab_lines = 0
        self.mixed_lines = 0
        self.indentation_sizes = {}

        stdout.write("%6s %6s %6s %4s  %s\n" % (
            "Space", "Tab", "Mixed", "Size", "File"))

    def process(self, rootpath):
        if os.path.isdir(rootpath):
            # It's a directory, process the files in it recursively.
            for dirpath, dirnames, files in os.walk(rootpath):
                for filename in files:
                    path = os.path.join(dirpath, filename)
                    self.scan_ws_file(path)
        else:
            # It's a file, scan it directly
            self.scan_ws_file(rootpath)

    def scan_ws_file(self, path):
        with open(path, "r") as f:
            self.scan_ws_stream(f, path)

    # Scan an input stream for the EOL types it has.
    def scan_ws_stream(self, f, name):
        indent_space_count = 0
        indent_tab_count = 0
        indent_mixed_count = 0
        size = 0

        for line in f:
            match = indent_pattern.search(line)
            if match:
                indent_str = match.group(1)
                sp_num = indent_str.count(" ")
                tab_num = indent_str.count("\t")
                indent_space_count += sp_num
                indent_tab_count += tab_num
                if sp_num > 0 and tab_num == 0:
                    if size == 0 or sp_num < size:
                        size = sp_num
                if sp_num > 0 and tab_num > 0:
                    indent_mixed_count += 1

        stdout.write("%6d %6d %6d %4d  %s\n" % (
            indent_space_count,
            indent_tab_count,
            indent_mixed_count,
            size,
            name))

        self.space_lines += indent_space_count
        self.tab_lines += indent_tab_count
        self.mixed_lines += indent_mixed_count
        if size not in self.indentation_sizes:
            self.indentation_sizes[size] = 1
        else:
            self.indentation_sizes[size] += 1

        if indent_space_count != 0 and indent_tab_count == 0 and indent_mixed_count == 0:
            self.space_files += 1
        elif indent_space_count == 0 and indent_tab_count != 0 and indent_mixed_count == 0:
            self.tab_files += 1
        elif indent_space_count == 0 and indent_tab_count == 0 and indent_mixed_count != 0:
            self.mixed_files += 1
        elif indent_space_count != 0 or indent_tab_count != 0 or indent_mixed_count != 0:
            self.mixed_files += 1
        else:
            self.none_files += 1

    def print_stats(self):
        stdout.write("\n")
        stdout.write("Lines:  spaces: {:8} tabs: {:8} mixed: {:8}\n".format(
                         self.space_lines, self.tab_lines, self.mixed_lines))
        stdout.write("Files:  spaces: {:8} tabs: {:8} mixed: {:8} none: {:8}\n".format(
                         self.space_files, self.tab_files, self.mixed_files, self.none_files))
        stdout.write("Sizes: {}\n".format(self.indentation_sizes))


### Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
