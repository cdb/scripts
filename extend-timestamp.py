#!/usr/bin/env python3
#
# Extend timestamps in log files down onto subsequent lines that don't have
# a timestamp, for instance, multiline stack traces.
# This makes it easier to merge multiple log files into a single
# chronological log.

import re
import sys

ts = '0000-00-00 00:00:00.000'
pat = re.compile(r'^(\d{4}-\d{2}-\d{2}[ T]\d{2}:\d{2}:\d{2}[,.]\d*)')
for line in sys.stdin:
    m = pat.match(line)
    if m:
        ts = m.group(1)
        sys.stdout.write(line)
    else:
        sys.stdout.write(ts + ' ' + line)
