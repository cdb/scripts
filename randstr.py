#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

import argparse
import logging
import os
import random
import re
import string
import subprocess
import sys


def main():
    # Setup command line parser
    parser = argparse.ArgumentParser(
            description="""
Generate random strings.
            """,
            formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument(
            "--count", type=int, default=1,
            help="number of strings")
    parser.add_argument(
            "--size", type=int, default=16,
            help="length of each string")
    parser.add_argument(
            "--chars", default=string.ascii_letters,
            help="characters to use")

    args = parser.parse_args()

    for i in range(0, args.count):
        sys.stdout.write(randomstr(args.size, args.chars))
        sys.stdout.write("\n")


def randomstr(n, a):
    """Return a random string of 'n' characters from 'a'.
    'a' must be a random-access collection such as 'str' or 'list'."""
    return "".join(randomlist(n, a))


def randomlist(n, a):
    """Return a list of 'n' random items from 'a'.
    'a' must be a collection providing random access, such as
    str or list."""
    return [randomitem(a) for i in range(0, n)]


def randomitem(a):
    """Return a random item from the collection 'a'.
    'a' must provide random access."""
    return a[random.randrange(0, len(a))]


# Run the main program

if __name__ == "__main__":
    main()


# vim: set ft=python et ts=4 sw=4:
