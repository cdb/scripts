#!/bin/sh
# Run a process, filtering output to make it more quiet,
# printing only lines matching a specific pattern, unless the process
# exits with an error code, in which case a portion of the final log
# output is printed in full.

CONCISE_FAILURE_LINES="${CONCISE_FAILURE_LINES:-200}"
CONCISE_PATTERN="${CONCISE_PATTERN:-}"

while true; do
    case "$1" in
        --failure-lines=*)
            CONCISE_FAILURE_LINES="${1#--failure-lines=}"
            shift
            ;;
        --pattern=*)
            CONCISE_PATTERN="${1#--pattern=}"
            shift
            ;;
        --)
            shift
            break
            ;;
        --*)
            echo "$0: error: invalid option $1" >&2
            exit 1
            ;;
        *)
            break
            ;;
    esac
done

logfile=$(mktemp --tmpdir concise.XXXXXX.log)

cleanup() {
    rm -f "$logfile"
}

trap cleanup EXIT

# A filter that writes all output to $logfile and only
# passes major output to stdout.
filter() {
    # The Buildroot make process prefixes major progress lines
    # such as downloading a package, configuring, and compiling it
    # with a ">>>" string.
    # We put all the lines to a log file, and only the important
    # lines to stdout for unconditional output.
    tee "$logfile" | { \
        if [ -n "$CONCISE_PATTERN" ]; then
            # Need --line-buffered, otherwise output will
            # be sporadic due to a ~4KB buffer.
            grep --line-buffered -e "$CONCISE_PATTERN"
        else
            # No pattern given, nothing but error output.
            cat > /dev/null
        fi
    }
}

# Usage: finish EXITCODE
# Takes an exit code, and if nonzero, prints the last
# $CONCISE_FAILURE_LINES of the detailed log file.
finish() {
    status="$1"
    if [ "$status" -ne 0 ]; then
        # Useless Use Of Cat to force buffer flush so the error
        # output header and the tail output go together.
        cat >/dev/null
        echo
        echo "----"
        echo
        echo "Error:"
        echo
        tail -n "$CONCISE_FAILURE_LINES" "$logfile"
    fi
    exit "$status"
}

(((("$@" 2>&1; echo $? >&3) | filter >&4) 3>&1) | (read xs; finish $xs)) 4>&1
